package main;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Main {
	public static void main(String[] args) throws IOException {
		while(true) {
			Cifra C = new Cifra(Input()); //2345678910
			C.CreateNums();
			String Number = C.SayNumber();
			System.out.println(C.getNumeroAString()+" : "+Number);
			ToFile("\n"+C.getNumeroAString()+" : "+Number,"Numeros.txt",true);
		}
	}
	private static String Input() throws IOException {
		System.out.println("Escribe un numero a traducir: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String String = br.readLine();
		if(CheckNum(String)) {
			return String;
			}
		else {
			System.out.println("Error, vuelve a intentarlo");
			return Input();
			}
		}

	private static boolean CheckNum(String String) {
		try {
			new BigInteger(String);
			return true;
		}
		catch(Exception NumberFormatException) {
			return false;
		}
	}

	private static void ToFile(String string,String Name, boolean Append) {
        try{
            FileWriter fw=new FileWriter(Name,Append);
            fw.write(string);
            fw.close();
        }
        catch(Exception e){System.out.println(e);}
        	System.out.println();
      	}
}	
	
