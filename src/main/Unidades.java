package main;

public class Unidades extends Numero {
	public Unidades(int Num) {
		super(Num);
		this.Cifratipo = 2;
		this.EnLetra = Unidades[Num];
	}
	public void setZero() {
		this.EnLetra = Unidades[0];
	}
	public void setEnLetra(String newAttribute) {
		this.EnLetra = newAttribute;
	}
}
