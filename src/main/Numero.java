package main;

public abstract class Numero implements CheckType {
	protected int Cifratipo;
	private int Num;
	protected String EnLetra = "";
	protected static String[] Unidades = {
			"Zero",
			"Un",
			"Dos",
			"Tres",
			"Cuatre",
			"Cinc",
			"Sis",
			"Set",
			"Vuit",
			"Nou",
			"Deu",
			"Onze",
			"Dotze",
			"Tretze",
			"Catorze",
			"Quinze",
			"Setze",
			"Deset",
			"Divuit",
			"Denou"
	};
	protected static String[] Decenas = {
			"[0]",
			"Deu",
			"Vint",
			"Trenta",
			"Quaranta",
			"Cinquanta",
			"Seixanta",
			"Setanta",
			"Vuitanta",
			"Noranta"
			};
	
	protected static String Cientos = "Cent";
	protected Numero(int Num) {
		this.setNum(Num);
	}
	public static Numero[][] CheckNumero(Numero[][] Array) {
		for(Numero[] N : Array) {
			Sintaxis(N);
		}
		return Array;
	}
	private static void Sintaxis(Numero[] Array) {
		int Count = 0;
		for(Numero N: Array) {
			if(N.Cifratipo == EsDecena && N.getNum() == 1) {
				Array[Count+1].setNum(Integer.parseInt((Integer.toString(N.getNum())+Integer.toString(Array[Count+1].getNum()))));
				Array[Count+1].updateEnLetra();
				N.setBlank();
			}
			else if(N.Cifratipo == EsDecena && N.getNum() == 2 && Array[Count+1].getNum() > 0) {
				N.EnLetra += "-i-";
			}
			else if(N.Cifratipo == EsDecena && N.getNum() > 2 && Array[Count+1].getNum() > 0) {
				N.EnLetra += "-";
			}
			Count += 1;
		}
		Count = 0;
	}
	public void setBlank() {
		this.EnLetra = "";
	}
	private void updateEnLetra() {
		this.EnLetra = Unidades[this.getNum()]+" ";
	}
	public int getNum() {
		return this.Num;
	}
	public void setNum(int num) {
		this.Num = num;
	}
}
