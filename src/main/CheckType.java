package main;

public interface CheckType {
	final int EsUnidad = 2; // Estos son los valores que tendria en un array EJ: unidad == 2 porque seria el tercer numero leido por la maquina
	final int EsDecena = 1;
	final int EsCentena = 0;
}
