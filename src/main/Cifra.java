package main;

public class Cifra implements CheckType {
	//private static String[] Escalas = {"","Mil","Millo","Billo","Trillo","Cuatrillo","Quintillo","Sixtillo","Septillo","Octillo","Nonillo","Decallo"}; ESTO ESTA MAL LOCO
	private static String[] Escalas = {"","Millo","Billo","Trillo","Cuatrillo","Quintillo","Sixtillo","Septillo","Octillo","Nonillo","Decallo"};
	private static String Miles = "Mil";
	
	private int[][] Array;
	private int Lenght;
	private String numeroAString;
	private String Shout;
	public Numero[][] NumerosArrayOBJ;
	
	public Cifra(String Integer) {
		this.Lenght = Integer.length();
		this.setNumeroAString(Integer);
		this.Array = DecomposeV2(Integer);
	}
	public String SayNumber() {
		String Shout = "";
		int CountEscalas = this.NumerosArrayOBJ.length-1;
		for(Numero[] Three: this.NumerosArrayOBJ) {
			for(Numero LookforZero : Three) {
				if(LookforZero.getNum() == 0 && this.Lenght > 1)
					LookforZero.setBlank();
			}
			CalcEscala(Three,CountEscalas%2 == 0 ? CountEscalas/2 : 0, CountEscalas%2 != 0 ? true : false);
			for(Numero Singles : Three) {
				Shout += Singles.EnLetra;
			}
			CountEscalas += -1;
		}
		Shout = Shout.replaceAll("\\s\\s\\s", " ").replaceAll("\\s\\s", " ");
		return Shout;
	}
	public Numero[][] CreateNums() {
		Numero[][] Array = InitDoubleArray();
		int CountX = 0; 
		for(int[] x : this.Array) {
			try {
				Array[CountX][EsUnidad] = new Unidades(x[EsUnidad]);
				Array[CountX][EsDecena] = new Decenas(x[EsDecena]);
				Array[CountX][EsCentena] = new Centenas(x[EsCentena]);
			}
			catch (Exception ArrayIndexOutOfBoundException1) {
				try {
					Array[CountX][1] = new Unidades(x[1]);
					Array[CountX][0] = new Decenas(x[0]);
				}
				catch (Exception ArrayIndexOutOfBoundException2) {
					Array[CountX][0] = new Unidades(x[0]);
				}
			}
			CountX += 1;
		}
		this.NumerosArrayOBJ = Numero.CheckNumero(Array);
		return Array;
	}
	private static String CalcEscala(Numero[] Unidades, int Escala, boolean Mil) {
		String NewString = "";
		String CheckPlus1 = "";
		int count = 0;
		for(Numero N : Unidades) {
			CheckPlus1 += Integer.toString(N.getNum());
			if (count == Unidades.length-1) {
				if(Integer.parseInt(CheckPlus1) > 0) {
					N.EnLetra += " "+Escalas[Escala];
					if(Integer.parseInt(CheckPlus1) > 1 && Escala != 0) {
						//if(Escala > 1) {
							N.EnLetra += "ns";
						//}
					}
					if(Mil) {
						N.EnLetra += Miles;
					}
					N.EnLetra += " ";
				}
			}
			
			count += 1;
		}
		return NewString;
	}
	
	private static int[][] DecomposeV2(String Numero) {
		char Decompose[] = new char[Numero.length()];
		Numero.getChars(0,Numero.length(),Decompose,0);
		int[][] NewArray = InitDoubleArray(Decompose.length);
		int Count = 0;
		for(int x = 0; NewArray.length > x; x++) {
			for(int y = 0; NewArray[x].length > y; y++) {
				NewArray[x][y] = Character.getNumericValue(Decompose[Count]);
				Count += 1;
			}
		}
		return NewArray;
	}
	
	public static int[][] InitDoubleArray(int Lenght) {
		int[][] NewArray = new int[Lenght % 3 > 0 ? Lenght/3+1:Lenght/3][];
		int counter = Lenght; 
		for(int i = 0 ; NewArray.length > i ; i++) {
			if(counter%3 != 0) {
				NewArray[i] = new int[counter%3];
				counter += -counter%3;
			}
			else {
				NewArray[i] = new int[3];
			}
		}
		return NewArray;
	}
	public Numero[][] InitDoubleArray() {
		Numero[][] NewArray = new Numero[this.Lenght % 3 > 0 ? this.Lenght/3+1:this.Lenght/3][];
		int counter = this.Lenght; 
		for(int i = 0 ; NewArray.length > i ; i++) {
			if(counter%3 != 0) {
				NewArray[i] = new Numero[counter%3];
				counter += -counter%3;
			}
			else {
				NewArray[i] = new Numero[3];
			}
		}
		return NewArray;
	}
	public String getNumeroAString() {
		return numeroAString;
	}
	public void setNumeroAString(String numeroAString) {
		this.numeroAString = numeroAString;
	}
	public String getShout() {
		return Shout;
	}
	public void setShout(String shout) {
		Shout = shout;
	}
}
