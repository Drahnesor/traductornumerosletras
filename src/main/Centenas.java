package main;

public class Centenas extends Numero{
	
	public Centenas(int Num) {
		super(Num);
		this.Cifratipo = 0;
		this.EnLetra = this.getNum() == 1 ? Cientos+" " : Unidades[Num]+"-"+Cientos+"s ";
	}
}
