package main;

public class Decenas extends Numero {
	private static String[] Decenas = {
			"[0]",
			"Deu",
			"Vint",
			"Trenta",
			"Quaranta",
			"Cinquanta",
			"Seixanta",
			"Setanta",
			"Vuitanta",
			"Noranta"
			};
	
	public Decenas(int Num) {
		super(Num);
		this.Cifratipo = 1;
		this.EnLetra = Decenas[Num];
	}
}
